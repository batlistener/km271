/*********
  Michael Meyer
  07.01.2022

  Installation:
  - Install Arduino IDE (I used the WIN 10 veriosn)
  - Install the ESP32-environment within the Arduino IDE. There are many websites explaining the process.
      e.g. https://www.az-delivery.de/blogs/azdelivery-blog-fur-arduino-und-raspberry-pi/esp32-jetzt-mit-boardverwalter-installieren
  - Select "ESP32 Dev Module" as device. You may leave all settings to default.
  - Use the Library administration (Sketch -> Include Libraries -> manage Libraries) and install the Telnet Stream Library.
  - Configure COM-Port for serial monitor and programming. Use 115200 Baud for serial monitor
  - Compile and programm, watch activities in serila monitor.

  Interfacing a KM271 board proposed by Daniel Glaser on
  https://the78mole.de/reverse-engineering-the-buderus-km217/

  This gets the values from the KM271/Ecomatic 2000 (the Buderus heating system) and makes them available as a list of
  javacript variables within a very tiny webserver located on port 82 (as default, check the settings to change the port).
  To access the javascript variables access
    http://your.ip.address:82/Param.js
  
*********/

// OTA library
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>                                 

// Reset reason handling
#include <rom/rtc.h>

// Telnet Stream for logging / debugging over Wifi
#include <TelnetStream.h>

// Appliaction includes
#include "time.h"
#include "km271_prot.h"
#include "webinterface.h"

// ************************ Configurations ****************************
#define MY_VERSION      "1.0.6"

// ********* Used UART-Pins to connect to KM271
#define RXD2            4                                 // ESP32 RX-pin for KM271 communication, align with hardware
#define TXD2            2                                 // ESP32 TX-pin for KM271 communication, align with hardware

// ********** WiFi
// Replace with your network credentials
static const char*      ssid      = "YourSSID";
static const char*      password  = "YourKey";
static const char*      hostname  = "EspHeizung";         // The name of this device for OTA mDNS

// ************************ Implementation ****************************

// This holds a reference to the event queue used between webinterface and KM271 module.
// The web interface initiates this queue and provides it to the KM271 task.
static QueueHandle_t      eventQueue;                           

// ********** Function prototypes
static void myLog(const char *txt);
static void myLogLn(const char *txt);
static String print_reset_reason(RESET_REASON reason);

// ********** Functions
/**
 *  ******************************************************
 * @brief   Is called by the system prior to loop() to give a chance
 *          for any initialization.
 *          
 * @details This initializes
 *            - serial interface 0 for logging / debugging
 *            - serial interface 2 for communication with Ecomatic 2000
 *            - WiFi connection is done
 *            - Time server and timezone is set up
 *            - Tasks are created to handle communication with Ecomatic 2000
 *
 * @param   none
 * @return  none
 */
void setup() {
  // Prepare logging / debug
  Serial.begin(115200);                                   // Debug/ logging interface on serial 0
  delay(1000);                                            // Make sure Serial is up and running before continuing. THis is to give Arduino-IDE time to re-open the COM port after flashing.

  // Connect to Wi-Fi network with SSID and password
  // Keep this in front of the km271 initialization.
  // This is quite specific to my own (old) server infrastructure and only to be seen as an example.
  if(webInterfaceInit(ssid, password, &eventQueue) != RET_OK) {   // Initializes the web interface.
    // Error to initialize web interface. doesn't make sense to continue...
    ESP.restart();                                        // Let us try to restart. No other option.
    return;
  }

  // From here on, we are connected to a WiFi network and local time is availble.
  // Start TelenetStream to enable debugging via Wifi network and telnet
  TelnetStream.begin();

  // Initialize the KM271 protocol communication.
  // This module is "talking" to the KM271/Logomatic. Hopefully, this can be re-used widely.
  // The web interface retrieves current information from this module.
  // In addition, this module generates events to the web interface.
  km271ProtInit(RXD2, TXD2, eventQueue);

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      myLog("Start updating ");
      myLogLn(type.c_str());
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      char buf[200];
      static unsigned int lastPercent;
      unsigned int        percent = (progress / (total / 100));
      // Log only ever full percent
      if(lastPercent != percent) {
        lastPercent = percent;
        sprintf(buf, "Progress: %u%%\r", percent); 
        myLog(buf);
      }
    })
    .onError([](ota_error_t error) {
      char buf[200];
      sprintf(buf, "Error[%u]: ", error);
      myLog(buf);
      if (error == OTA_AUTH_ERROR) myLogLn("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) myLogLn("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) myLogLn("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) myLogLn("Receive Failed");
      else if (error == OTA_END_ERROR) myLogLn("End Failed");
    });

  ArduinoOTA.setHostname(hostname);
  ArduinoOTA.begin();
}

/**
 *  ******************************************************
 * @brief   Is called by the system prior as main loop of the main task.
 *          
 * @details This mainly does: NOTHING
 *          All functionality is handled in the tasks set up in the 
 *          respective modules.
 *          The modules are initialized by setup();
 *
 * @param   none
 * @return  none
 */
void loop(){
  // Allow reset via Telnet. Important for OTA. mDNS is working during reset only!?
  // To see the device in the Arduino-IDE, it might be necessary to reset the device while the Arduino IDE is active.
  switch(TelnetStream.read()) {
    case 'R':                                         // Reset
      myLogLn("Executing reset now!");
      myLogLn("Re-connect to see logging again!");
      TelnetStream.stop();
      delay(100);
      ESP.restart();
      break;
    case 'C':                                           // Just check communication
      myLogLn("bye bye");
      TelnetStream.stop();                              // This does not really stop. Communication is continuing.
      break;
    case 'T':                                           // Current task stack values and last reset reason

      // Provide task status information

      // Version of this sketch
      TelnetStream.print("\n\nVersion: ");
      TelnetStream.println(MY_VERSION);

      // Reset reasons
      TelnetStream.print("\nCPU0 reset reason: ");
      TelnetStream.println(print_reset_reason(rtc_get_reset_reason(0)));

      TelnetStream.print("CPU1 reset reason: ");
      TelnetStream.println(print_reset_reason(rtc_get_reset_reason(1)));
      
      TelnetStream.print("Main loop stack: ");
      TelnetStream.println(uxTaskGetStackHighWaterMark( NULL ));                        

      TelnetStream.print("Km271 RX stack: ");
      TelnetStream.println(uxTaskGetStackHighWaterMark(hKmRxTaskHandle));                        

      TelnetStream.print("Web status stack: ");
      TelnetStream.println(uxTaskGetStackHighWaterMark(hWebStatusTaskHandle));                        

      TelnetStream.print("Web update stack: ");
      TelnetStream.println(uxTaskGetStackHighWaterMark(hWebUpdateTaskHandle));                        

      TelnetStream.print("Web event stack: ");
      TelnetStream.println(uxTaskGetStackHighWaterMark(hWebEventTaskHandle));                        

      // ESP heap
      TelnetStream.print("ESP free heap: ");
      TelnetStream.println(ESP.getFreeHeap());                        

      TelnetStream.print("Wifi-RSSI: ");
      TelnetStream.println(WiFi.RSSI());

      TelnetStream.print("\n\n");

      break;
    default:
      break;
  }
  ArduinoOTA.handle();
  vTaskDelay(200 / portTICK_PERIOD_MS);                 // This allows the other tasks to work...
}

/**
 *  ******************************************************
 * @brief   Maps logging to the desired streams.
 * @details This gives a bit more flexibility to logging.
 *          Currently, logging is provided to Serial and TelnetStream in parallel.
 *          This can definitively solved more elegant, but this simple c-style solution works, too...
 * @param   txt: The string to log
 * @return  none
 */
static void myLog(const char *txt) {
  Serial.print(txt);                                    // Provide to serial debugging interface 
  TelnetStream.print(txt);                              // Provide to Telnet stream (if active)
}
static void myLogLn(const char *txt) {
  Serial.println(txt);                                  // Provide to serial debugging interface 
  TelnetStream.println(txt);                            // Provide to Telnet stream (if active)
}

static String print_reset_reason(RESET_REASON reason) {
  switch ( reason) {
    case 1 : return ("POWERON_RESET");break;          /**<1,  Vbat power on reset*/
    case 3 : return ("SW_RESET");break;               /**<3,  Software reset digital core*/
    case 4 : return ("OWDT_RESET");break;             /**<4,  Legacy watch dog reset digital core*/
    case 5 : return ("DEEPSLEEP_RESET");break;        /**<5,  Deep Sleep reset digital core*/
    case 6 : return ("SDIO_RESET");break;             /**<6,  Reset by SLC module, reset digital core*/
    case 7 : return ("TG0WDT_SYS_RESET");break;       /**<7,  Timer Group0 Watch dog reset digital core*/
    case 8 : return ("TG1WDT_SYS_RESET");break;       /**<8,  Timer Group1 Watch dog reset digital core*/
    case 9 : return ("RTCWDT_SYS_RESET");break;       /**<9,  RTC Watch dog Reset digital core*/
    case 10 : return ("INTRUSION_RESET");break;       /**<10, Instrusion tested to reset CPU*/
    case 11 : return ("TGWDT_CPU_RESET");break;       /**<11, Time Group reset CPU*/
    case 12 : return ("SW_CPU_RESET");break;          /**<12, Software reset CPU*/
    case 13 : return ("RTCWDT_CPU_RESET");break;      /**<13, RTC Watch dog Reset CPU*/
    case 14 : return ("EXT_CPU_RESET");break;         /**<14, for APP CPU, reseted by PRO CPU*/
    case 15 : return ("RTCWDT_BROWN_OUT_RESET");break;/**<15, Reset when the vdd voltage is not stable*/
    case 16 : return ("RTCWDT_RTC_RESET");break;      /**<16, RTC Watch dog reset digital core and rtc module*/
    default : return ("NO_MEAN");
  }
}

  
