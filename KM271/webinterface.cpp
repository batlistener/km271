//*****************************************************************************
//
// File Name  : 'webinterface.cpp'
// Title      : Handles the webinterfaces for homeautomation M.Meyer.
//              Just to be seen as example. This is highly specific for a specific, already
//              existing infrastructure.
// 
//              There are 3 web based interfaces handled:
//              1)
//              A web server client interface. You can simply address the webserver under its local or public
//              address and port (see configuration below). The web server replies with a javescrip page containing
//              the currently available KM271-readings. The delivered javascript sniplett can easily be incorporated in
//              any web page to display the data or further process it. This is especially interesting for debugging
//              purposes.
//              2)
//              A cyclic push. Every 10 minutes a dedicated task within this modules provides a certain
//              subset of the KM271 data to a certain web address. In my case, this is used to put the current
//              KM271 data into a server based database.
//              3)
//              Event logging. Certain KM271 data is considered to be an "event". E.g. when pumps are switched
//              on or off. Many events are combined into a common event bit list. Whenever one of the events has changed
//              a certain web address is called with the current event list to put these events into a
//              a server base database whenever they change.
//
//              Again, all of this quite proprietary to my installation, which exits already for more than 10 years.
//              This implementation aims to be a "plug-in-replacement" for that existing implementation.
//              Neverthesless, you might see this as an useful example and starting point to make it better...
// 
//              Interface 1) and 2) just poll the km271_prot. They call the respective function in the KM271
//              module to get a copy of the current set of available KM271 information, re-format it according to the
//              server requirements and send it out.
//              Interface 3) is event driven. Whenever an event changes, the KM271 actively generates a queue entry this
//              module is waiting for. This module then reformats and send the event to the server.
//              For this to work, the KM271 needs to know the event-queue it shall use to inform this
//              module. This information is exchanged during initialization.
//
// Author     : Michael Meyer
// Created    : 20.01.2022
// Version    : 0.1
// Target MCU : ESP32/Arduino
// Indicator  : km
//
//
//*****************************************************************************

/** I N C L U D E S **********************************************************/
#include "Arduino.h"
#include <WiFi.h>
#include <WebServer.h>
#include <HTTPClient.h>
#include "km271_prot.h"
#include "webinterface.h"

// Telnet Stream for logging / debugging over Wifi
#include <TelnetStream.h>

/* D E C L A R A T I O N S **************************************************/
// Configuration
// ********** Web Server
static const int      webServerPort         = 82;                                   // The port the webserver shall listen to
static char*          parameterPage         = "/Param.js";                          // KM271 parameters are delivered when someone requests this page on this webserver

// Address to push HTTP POSTs to:
String                serverName            = "";                                   // Use empty String to disable POST to server
String                serverOkResp          = "OK_DB";                              // The string provided by the server to confirm storage to data base
#define               DBERR_RTY_MS          5000                                    // 5 seconds retry delay if database update fails
#define               MAX_HTTP_RETRY        3                                       // Number of retries when acces to DB falis
#define               HTTP_CONNECT_TMO      15000                                   // HTTP connect timeout
#define               HTTP_TMO              20000                                   // HTTP timeout. How long to wait for a response. Keep quite long as database may be slow.

// ********** Time service configuration
// By default the system time is kept up-to-date automatically.
static const char*    ntpServer = "pool.ntp.org";                            // Time server to be used
static const long     gmtOffset_sec = 0;                                     // Offset handled by TZ_INFO
static const int      daylightOffset_sec = 0;                                // Offset handled by TZ_INFO
static const char*    TZ_INFO = "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00";  // TZ_INFO for Germany

// Save Web credentials for re-connect
static const char*    my_ssid;
static const char*    my_password;

/* V A R I A B L E S ********************************************************/
QueueHandle_t         km271EventQueue;

// For debugging, e.g. check available stack
TaskHandle_t          hWebStatusTaskHandle;                                   // To store the web status request task handle
TaskHandle_t          hWebUpdateTaskHandle;                                   // To store the web update task handle
TaskHandle_t          hWebEventTaskHandle;                                    // To store the web event request task handle

// ********** Local variables
static WebServer      server(webServerPort);                                  // Set web server port number to the configured port number
static const char*    deWeekdays[] = {                                        // To get german week day abbreviations. setlocale() seems not to be supported...
                        "So",                                                 // 0
                        "Mo",                                                 // 1
                        "Di",                                                 // 2
                        "Mi",                                                 // 3
                        "Do",                                                 // 4
                        "Fr",                                                 // 5
                        "Sa",                                                 // 6
                      };

/* Local F U N C T T I O N  P R T O T Y P E S *******************************/
static void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info);
static void myLog(const char *txt);
static void myLogLn(const char *txt);
static void WiStatusRequestTask(void * parameter);
static void WiStatusUpdateTask(void * parameter);
static void WiEventTask(void * parameter);
static void printLocalTime();
static e_ret postToServer(char *requestData);
static void handle_Km271StatusRequest();
static String buildKm271StatusRequestString();
static String buildKm271StatusDbUpdateString();
static String buildKm271StatusDbUpdateEventString(s_km271_event_queue *ev);
static QueueHandle_t eventQueue;
static void (*dbgLog)(String);

/* C O D E ******************************************************************/

/**
 *  ******************************************************
 * @brief   Initializes the webinterface
 * @details This tries to connect to the local Wifi network and starts the web server.
 *          In addition, the system time is retrieved from the internet.
 * @param   ssid:     The Wifi SSID to be used
 * @param   password  The Wifi password to be used
 * @param   eventQ    Pointer to provide the KM272 module a handle to the event queue this module is using to wait for evenets.
 * @return  e_ret error code
 */
e_ret webInterfaceInit(const char *ssid, const char * password, QueueHandle_t *eventQ) {
  char    buf[100];

  // Save pointers to Wifi login credentials
  my_ssid = ssid;
  my_password = password;
  
  myLog("\n\nConnecting to ");
  myLogLn(my_ssid);

  WiFi.disconnect(true);                                      // Delete current configuration
  delay(1000);                                                // Wait a bit to make sure, we are disconnected

  // Setup call backs to handle connect / re-connect
  WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);

  // Try to connect now
  WiFi.begin(my_ssid, my_password);

  // Wait until connection gets established
  while (WiFi.status() != WL_CONNECTED) {                     // Wait for connection.
    delay(500);                                               // It does not make sense to continue, until connection is made as we do not store local values.
    myLog(".");                                               // Just to show that we are alive...
  }

  // Print local IP address and start web server
  myLogLn("");
  myLogLn("WiFi connected.");
  myLogLn("Access web interface using: ");
  myLog("    ");
  myLog(WiFi.localIP().toString().c_str());
  myLog(":");
  sprintf(buf, "%d", webServerPort);
  myLog(buf);
  myLogLn(parameterPage);
  // Do not start the web server before having a valid time available.
  // The time is delivered by the webserver!

  // Set up time by NTP
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);   // Configure time server
  setenv("TZ", TZ_INFO, 1);                                   // Set time zone info
  tzset();                                                    // Set time zone info
  printLocalTime();                                           // Just make sure, we have valid time

  // Setup the handling of KM271 requests.
  // This function will be called, if someone requests the parameter page, e.g.
  //  192.168.178.48:82/Param.js
  // The page to be looking for is to be configured above!
  server.on(parameterPage, handle_Km271StatusRequest);

  // Now we may start the web server.
  // Data from heating may not be availabl, yet, but this is considered as normal.
  server.begin();                                             // From here on the webserver is running

  // Server is running. So setup the tasks required to handle the web interface.
  
  // First the task to handle KM271 status requests.
  // This taks just feeds the built-in web server.
  if (xTaskCreatePinnedToCore(
      WiStatusRequestTask,  // Function that should be called
      "WiStatusRequest",    // Name of the task (for debugging)
      5000,                 // Stack size (bytes)
      NULL,                 // Parameter to pass
      1,                    // Task priority
      &hWebStatusTaskHandle, // Task handle
      1                     // Use core 1 always (same as loop() is using. Do not interfere with WLAN, which runs on core 0.
      ) != pdTRUE) {
    myLogLn("Cannot create Web Interface status request task!!! Aborting....");
    return RET_ERR;
  }

  // Second task to handle proactive KM271 status updates every 10 minutes
  if (xTaskCreatePinnedToCore(
      WiStatusUpdateTask,   // Function that should be called
      "WiStatusUpdate",     // Name of the task (for debugging)
      5000,                 // Stack size (bytes)
      NULL,                 // Parameter to pass
      1,                    // Task priority
      &hWebUpdateTaskHandle, // Task handle
      1                     // Use core 1 always (same as loop() is using. Do not interfere with WLAN, which runs on core 0.
      ) != pdTRUE) {
    myLogLn("Cannot create Web Interface status update task!!! Aborting....");
    return RET_ERR;
  }

  // Create the event queue to be able to wait for events from KM271 module
  eventQueue = xQueueCreate(10, sizeof(s_km271_event_queue));
  if(eventQueue == NULL) {
    myLogLn("Cannot create event queue!!! Aborting....");
    return RET_ERR;
  }
  // Deliver the queue to the KM271 module
  *eventQ = eventQueue;

  // Third task to handle event driven KM271 event updates
  if (xTaskCreatePinnedToCore(
      WiEventTask,          // Function that should be called
      "WiEvent",            // Name of the task (for debugging)
      5000,                 // Stack size (bytes)
      NULL,                 // Parameter to pass
      1,                    // Task priority
      &hWebEventTaskHandle, // Task handle
      1                     // Use core 1 always (same as loop() is using. Do not interfere with WLAN, which runs on core 0.
      ) != pdTRUE) {
    myLogLn("Cannot create Web Interface event task!!! Aborting....");
    return RET_ERR;
  }

  return RET_OK;
}


/**
 *  ******************************************************
 * @brief   Handle disconnect and re-connect
 * @details This is called, when the Wifi connection gets diconnected.
 *          It tries to re-connect immediately.
 * @param   parameter   As delivered by ESP32
 * @return  none
 */
static void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info){
  // As we are not connected, it does not make sense to user telnet serial here to log information...
  Serial.println("\nDisconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.println("Trying to Reconnect");
  WiFi.begin(my_ssid, my_password);
}

/**
 *  ******************************************************
 * @brief   The web interface web server task.
 * @details This keep the web server active and allows to handle
 *          events such as requests.
 * @param   parameter   Optional pointer to task parameters (unused)
 * @return  none
 */
static void WiStatusRequestTask(void * parameter){
  for(;;){
    vTaskDelay(50 / portTICK_PERIOD_MS);      // Important! This feeds the Watchdog. See https://github.com/espressif/arduino-esp32/issues/595
    server.handleClient();                    // Handle any client connection requests in the web server
  }
}

/**
 *  ******************************************************
 * @brief   A KM271 status request was received by the webserver
 *          This function delivers the current KM271 status as a javascript
 *          response          
 * @details This generates necessary HTML header information and then 
 *          responds with the response string build by buildKm271StatusRequestString();
 *
 * @param   none
 * @return  none
 */
static void handle_Km271StatusRequest() {
  myLogLn("Reply to KM271 status request from web.");          
  server.sendHeader("Cache-Control", "no-cache");
  server.sendHeader("Pragma", "no-cache");
  server.send(200, "application/x-javascript", buildKm271StatusRequestString()); 
}

/**
 *  ******************************************************
 * @brief   Builds the KM271 status response string
 * @details The response is built as a list of javascript variables.
 *
 * @param   none
 * @return  The response string, formatted as needed
 */
static String buildKm271StatusRequestString() {
  String          ptr = "";                       // To collect the resonse string
  char            buf[256];                       // String buffer for general usage
  struct tm       timeinfo;                       // For time handling
  s_km271_status  kmStatus;                       // To store a copy of the current status

  // Retrieve all the current values from km271 module
  km271GetStatus(&kmStatus);                              // Now we can work on kmStatus safely
  // Retrieve current local date/time.
  if(!getLocalTime(&timeinfo)){
    ptr += "var HDATE=\"Mi, 01.01.1900\";\n";             // Respond with a dummy date
    ptr += "var HTIME=\"00:00:00\";\n";                   // Respond with dummy time
  } else {
    // Generate date string with German short weekday coding. Quite ridicolous solution! Any better way to do this?
    sprintf(buf,"var HDATE=\"%s, ", deWeekdays[timeinfo.tm_wday]);    // Pre-text and the german weekday translation
    strftime(buf + strlen(buf), sizeof(buf),"%d.%m.%Y",&timeinfo);    // The date is appended in the required format (German again)
    strcat(buf, "\";\n");                                 // Append closing string
    ptr += buf;                                           // Deliver date string
    strftime(buf, sizeof(buf), "var HTIME=\"%H:%M:%S\";\n", &timeinfo);// Respond with current local time   
    ptr += buf;
  }

  ptr += "var HBW1=\"";
  sprintf(buf,"%02X\";\n",kmStatus.HeatingCircuitOperatingStates_1);
  ptr += buf;
  
  ptr += "var HBW2=\"";
  sprintf(buf,"%02X\";\n",kmStatus.HeatingCircuitOperatingStates_2);
  ptr += buf;
  
  ptr += "var HVLI=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.HeatingForwardActualTemp);
  ptr += buf;
  
  ptr += "var HVLS=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.HeatingForwardTargetTemp);
  ptr += buf;

  ptr += "var HRS=\"";
  sprintf(buf,"%u,%u\";\n", (int)kmStatus.RoomTargetTemp,((int)(kmStatus.RoomTargetTemp * 10))%10);
  ptr += buf;
  
  ptr += "var HRI=\"";
  sprintf(buf,"%u,%u\";\n", (int)kmStatus.RoomActualTemp,((int)(kmStatus.RoomActualTemp * 10))%10);
  ptr += buf;
  
  ptr += "var HEOZ=\"";
  sprintf(buf,"%d\";\n", kmStatus.SwitchOnOptimizationTime);
  ptr += buf;

  ptr += "var HAOZ=\"";
  sprintf(buf,"%d\";\n", kmStatus.SwitchOffOptimizationTime);
  ptr += buf;
  
  ptr += "var HPUMP=\"";
  sprintf(buf,"%d\";\n", kmStatus.PumpPower);
  ptr += buf;

  ptr += "var HMSCH=\"";
  sprintf(buf,"%d\";\n", kmStatus.MixingValue);
  ptr += buf;
  
  ptr += "var HK10=\"";
  sprintf(buf,"%d\";\n", (int)kmStatus.HeatingCurvePlus10);
  ptr += buf;
  
  ptr += "var HK0=\"";
  sprintf(buf,"%d\";\n", (int)kmStatus.HeatingCurve0);
  ptr += buf;

  ptr += "var HKM10=\"";
  sprintf(buf,"%d\";\n", (int)kmStatus.HeatingCurveMinus10);
  ptr += buf;
  
  ptr += "var HWWBW1=\"";
  sprintf(buf,"%02X\";\n",kmStatus.HotWaterOperatingStates_1);
  ptr += buf;

  ptr += "var HWWBW2=\"";
  sprintf(buf,"%02X\";\n",kmStatus.HotWaterOperatingStates_2);
  ptr += buf;

  ptr += "var HWWST=\"";
  sprintf(buf,"%d\";\n", (int)kmStatus.HotWaterTargetTemp);
  ptr += buf;
  
  ptr += "var HWWIT=\"";
  sprintf(buf,"%d\";\n", (int)kmStatus.HotWaterActualTemp);
  ptr += buf;

  ptr += "var HWWOZ=\"";
  sprintf(buf,"%d\";\n", kmStatus.HotWaterOptimizationTime);
  ptr += buf;
    
  ptr += "var HWWLP=\"";
  sprintf(buf,"%02X\";\n",kmStatus.HotWaterPumpStates);
  ptr += buf;

  ptr += "var HKVST=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.BoilerForwardTargetTemp);
  ptr += buf;
 
  ptr += "var HKVIT=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.BoilerForwardActualTemp);
  ptr += buf;

  ptr += "var HBEST=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.BurnerSwitchOnTemp);
  ptr += buf;

  ptr += "var HBAST=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.BurnerSwitchOffTemp);
  ptr += buf;

  ptr += "var HKI1=\"";
  sprintf(buf,"%d\";\n",kmStatus.BoilerIntegral_1);
  ptr += buf;
  
  ptr += "var HKI2=\"";
  sprintf(buf,"%d\";\n",kmStatus.BoilerIntegral_2);
  ptr += buf;

  ptr += "var HKFEH=\"";
  sprintf(buf,"%02X\";\n",kmStatus.BoilerErrorStates);
  ptr += buf;

  ptr += "var HKBET=\"";
  sprintf(buf,"%02X\";\n",kmStatus.BoilerOperatingStates);
  ptr += buf;

  ptr += "var HBREN=\"";
  sprintf(buf,"%02X\";\n",kmStatus.BurnerStates);
  ptr += buf;
    
  ptr += "var HABGS=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.ExhaustTemp);
  ptr += buf;
  
  ptr += "var HHABGS=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.MaxExhaustTemp);
  ptr += buf;
  
  ptr += "var HBLZ2=\"";
  sprintf(buf,"%d\";\n", kmStatus.BurnerOperatingDuration_2);
  ptr += buf;

  ptr += "var HBLZ1=\"";
  sprintf(buf,"%d\";\n", kmStatus.BurnerOperatingDuration_1);
  ptr += buf;

  ptr += "var HBLZ0=\"";
  sprintf(buf,"%d\";\n", kmStatus.BurnerOperatingDuration_0);
  ptr += buf;

  ptr += "var HAUSS=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.OutsideTemp);
  ptr += buf;
  
  ptr += "var HAUSG=\"";
  sprintf(buf,"%d\";\n",(int)kmStatus.OutsideDampedTemp);
  ptr += buf;

  ptr += "var HHVER=\"";
  sprintf(buf,"%d\";\n", kmStatus.ControllerVersionMain);
  ptr += buf;
 
  ptr += "var HUVER=\"";
  sprintf(buf,"%d\";\n", kmStatus.ControllerVersionSub);
  ptr += buf;

  ptr += "var HMOD=\"";
  sprintf(buf,"%d\";\n", kmStatus.Modul);
  ptr += buf;
  
  ptr += "var HHMTMR=\"0\";\n";
  ptr += "var HHMKM=\"0\";\n";
  ptr += "var HHMUPD=\"0\";\n";
  ptr += "var HHMHEAP=\"0\";\n";
  ptr += "var HHMRES=\"0\";\n";
  ptr += "var HHMSPI=\"0\";\n";
  ptr += "var HHMRXE=\"0\";\n";
  
  ptr += "var HEVST=\"";
  sprintf(buf,"%u\";\n", kmStatus.EventBitList);
  ptr += buf;

  return ptr;  
}


/**
 *  ******************************************************
 * @brief   Builds the KM271 status response string
 * @details The response is built as a list of javascript variables.
 *
 * @param   none
 * @return  The response string, formatted as needed
 */
static String buildKm271StatusDbUpdateString() {
  String          ptr = "";                       // To collect the resonse string
  char            buf[256];                       // String buffer for general usage
  struct tm       timeinfo;                       // For time handling
  s_km271_status  kmStatus;                       // To store a copy of the current status

  // Retriev all the current values from km271 module
  km271GetStatus(&kmStatus);                              // Now we can work on kmStatus safely
  // Retrieve current local date/time.
  if(!getLocalTime(&timeinfo)){
    ptr += "D=1900-01-01";                                // Respond with a dummy date
    ptr += "Z=00:00:00";                                  // Respond with dummy time
  } else {
    // Generate date and time string in SQL format
    // Set seconds to 0 always. It is good enough to keep the 10 minutes slots...
    strftime(buf, sizeof(buf),"D=%Y-%m-%d",&timeinfo);    // The date is appended in the required SQL format
    ptr += buf;                                           // Deliver date string
    strftime(buf, sizeof(buf), "&Z=%H:%M:00", &timeinfo); // Respond with current local time, set seconds to 0
    ptr += buf;
  }
  // Add the current information from the km271 status in POST-Format
  
  sprintf(buf,"&TIR=%0.1f", kmStatus.RoomActualTemp);
  ptr += buf;

  sprintf(buf,"&TSR=%0.1f", kmStatus.RoomTargetTemp);
  ptr += buf;

  sprintf(buf,"&TIA=%d", (int)kmStatus.OutsideTemp);
  ptr += buf;

  sprintf(buf,"&TVA=%d", (int)kmStatus.OutsideDampedTemp);
  ptr += buf;

  sprintf(buf,"&TIKV=%d", (int)kmStatus.BoilerForwardActualTemp);
  ptr += buf;

  sprintf(buf,"&TSKV=%d", (int)kmStatus.BoilerForwardTargetTemp);
  ptr += buf;

  sprintf(buf,"&TBRE=%d", (int)kmStatus.BurnerSwitchOnTemp);
  ptr += buf;

  sprintf(buf,"&TBRA=%d", (int)kmStatus.BurnerSwitchOffTemp);
  ptr += buf;

  sprintf(buf,"&TIV=%d", (int)kmStatus.HeatingForwardActualTemp);
  ptr += buf;

  sprintf(buf,"&TSV=%d", (int)kmStatus.HeatingForwardTargetTemp);
  ptr += buf;

  sprintf(buf,"&TIWW=%d", (int)kmStatus.HotWaterActualTemp);
  ptr += buf;

  sprintf(buf,"&TSWW=%d", (int)kmStatus.HotWaterTargetTemp);
  ptr += buf;

  sprintf(buf,"&TIABG=%d", (int)kmStatus.MaxExhaustTemp);
  ptr += buf;

  sprintf(buf,"&TIHKP10=%d", (int)kmStatus.HeatingCurvePlus10);
  ptr += buf;

  sprintf(buf,"&TIHK0=%d", (int)kmStatus.HeatingCurve0);
  ptr += buf;

  sprintf(buf,"&TIHKM10=%d", (int)kmStatus.HeatingCurveMinus10);
  ptr += buf;
  
  return ptr;  
}

/**
 *  ******************************************************
 * @brief   The web interface update status task.
 * @details This proactively updates the KM271 status on an external
 *          web site every 10 Minutes
 * @param   parameter   Optional pointer to task parameters (unused)
 * @return  none
 */
static void WiStatusUpdateTask(void * parameter){
  String          reqBuf;                                         // To store a string send as HTTP-POST do database
  int             retryCnt;                                       // Do some retries if connection to server fails
  struct tm       timeinfo;                                       // For time handling
  int             alreadySent;                                    // Flag to handle timing

  alreadySent = 0;                                                // Reset sent flag
    
  for(;;){
    vTaskDelay(5000 / portTICK_PERIOD_MS);                        // Poll timing every 5 seconds. Good enough!
    
    if(getLocalTime(&timeinfo)){                                  // Get actual time. Use it only if valid
      // Update database every 10 minutes, i.e. when the number of minutes can be divided by 10 without rest
      if(timeinfo.tm_min%10 == 0) {
        if(!alreadySent) {
          alreadySent = 1;                                        // Send only once per 10 minutes
          // Special case: Every midnight, we reset the maximum exhaust temperature collected during the day.
          if(!timeinfo.tm_min && !timeinfo.tm_hour) {
            km271ResetDailyValues();                              // Reset all daily collected values.
          }
          reqBuf = buildKm271StatusDbUpdateString();              // Get latest status from KM and format it accordingly
          retryCnt = MAX_HTTP_RETRY;                              // Prepare retry count, if needed
          while(retryCnt) {                                       // Retry, if needed
            if(postToServer(&reqBuf[0]) == RET_OK) {              // Send the new KM271 information to server using POST
              break;                                              // Sent successfully, done
            }
            retryCnt--;                                           // Ooops, something went wrong. Try again...
            vTaskDelay(DBERR_RTY_MS / portTICK_PERIOD_MS);        // Retry delay. Wait a bit until next retry.
          }
        }
      } else {
        alreadySent = 0;                                        // Not in tenth minutes any more, reset flag
      }
    }
  }
}


/**
 *  ******************************************************
 * @brief   Builds the KM271 status response string for event update.
 * @details The response is built as a list of javascript variables.
 *
 * @param   ev. The event to be sent to the server
 * @return  The response string, formatted as needed
 */
static String buildKm271StatusDbUpdateEventString(s_km271_event_queue *ev) {
  String          ptr = "";                       // To collect the resonse string
  char            buf[256];                       // String buffer for general usage
  struct tm       ts;                             // For event time handling

  // Retrieve event local date/time.
  ts = *localtime(&ev->uxTimestamp);
  // Generate date and time string in SQL format
  strftime(buf, sizeof(buf),"D=%Y-%m-%d",&ts);          // The date is appended in the required SQL format
  ptr += buf;                                           // Deliver date string
  strftime(buf, sizeof(buf), "&Z=%H:%M:%S", &ts);       // Respond with current local time   
  ptr += buf;

  // Add the current information from the km271 status in POST-Format
  // In this case just add the event list from the received event.
  sprintf(buf,"&EV=%lu", ev->eventBitList);
  ptr += buf;

  return ptr;  
}

/**
 *  ******************************************************
 * @brief   The web interface event task.
 * @details This waits for events delivered by KM271 module and 
 *          proactively updates the KM271 events on an external
 *          web site
 * @param   parameter   Optional pointer to task parameters (unused)
 * @return  none
 */
static void WiEventTask(void * parameter){
  s_km271_event_queue newEvent;
  char                buf[200];
  int                 retryCnt;                               // Do some retries if connection to server fails
  String              reqBuf;                                 // To store a string send as HTTP-POST do database
  struct tm           ts;                                     // For event time handling

  for(;;){
    // Wait for next event list reported by KM271
    if(xQueueReceive(eventQueue, &newEvent, portMAX_DELAY) == pdTRUE) {
      reqBuf = buildKm271StatusDbUpdateEventString(&newEvent);// Format the event data for POST
      retryCnt = MAX_HTTP_RETRY;                              // Prepare retry count, if needed
      while(retryCnt) {                                       // Retry, if needed
        if(postToServer(&reqBuf[0]) == RET_OK) {              // Send the new KM271 information to server using POST
          break;                                              // Sent successfully, done
        }
        retryCnt--;                                           // Ooops, something went wrong. Try again...
        vTaskDelay(DBERR_RTY_MS / portTICK_PERIOD_MS);        // Retry delay. Wait a bit until next retry.
      }
      // Do some local logging
      ts = *localtime(&newEvent.uxTimestamp);
      strftime(buf, sizeof(buf), "Event time: %a %Y-%m-%d %H:%M:%S %Z", &ts);
      myLogLn(buf);
      sprintf(buf, "Event value: 0x%08X", newEvent.eventBitList);
      myLogLn(buf);
    }
  }
}


/**
 *  ******************************************************
 * @brief   Prints the local time ion english format
 *          
 * @details This is used to provide the current system time in readable format
 *          on the debug console.
 *
 * @param   none
 * @return  none
 */
static void printLocalTime() {
  struct tm timeinfo;
  char      buf[200];
  
  if(!getLocalTime(&timeinfo)){
    myLogLn("Failed to obtain time");
    return;
  }
  strftime(buf, sizeof(buf),"%A, %B %d %Y %H:%M:%S", &timeinfo);
  myLogLn(buf);
}

/**
 *  ******************************************************
 * @brief   POST a query string to the configured server and address
 * @details 
 * @param   requestData : The query string to be posted
 * @return  RET_OK, if delivery is successfull
 *          It will be checked if the website could be reached and also, if the 
 *          data could be stored in data base (according to the delivered confirmation
 *          in body.
 */
static e_ret postToServer(char *requestData) {
  e_ret     ret = RET_OK;
  int       timeoutMs = (2 * 60 * 1000);                        // Wait up to 2 minutes for a connection, As 3 re-tries are derfined, this time * 3 should be < 10 minutes
  char      buf[200];

  // Only try to POST, when there is a server set
  if(serverName.length() == 0) {                                // No server set
    return ret;                                                 // Ignore by relying with OK
  }

  // Wait until connection gets established
  while ((timeoutMs > 0) && (WiFi.status() != WL_CONNECTED)) {  // Wait for connection.
    delay(500);                                                 // It does not make sense to continue, until connection is made as we do not store local values.
    timeoutMs -= 500;                                           // Decrement timeout to make sure we break tghis loop after timeout time
  }
  
  // Only try to send if finally connected, 
  if(WiFi.status()== WL_CONNECTED){                             // Only try to send if connected to Wifi network
    WiFiClient client;                                          // Prepare client operation
    HTTPClient http;                                            // Prepare HTTP operation
    
    http.begin(client, serverName);
    // Specify content-type header
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    http.setConnectTimeout(HTTP_CONNECT_TMO);                   // Wait up to 15 senconds for a response
    http.setTimeout(HTTP_TMO);                                  // Wait up to 20 seconds. The server and the database it is quite slow sometimes...
    // Send HTTP POST request
    int httpResponseCode = http.POST(requestData);
    if(httpResponseCode != 200) {
      myLog("HTTP ERROR: HTTP Response code: ");
      sprintf(buf, "%d", httpResponseCode);
      myLogLn(buf);
      ret = RET_ERR;
    } else {
      // Check, if the website has delivered "OK_DB" as confirmation that data has been written to data base
      String resp = http.getString();                         // Get the response to the request
      if (!resp.startsWith(serverOkResp)) {                   // Check for data base confirmation
        myLog("HTTP ERROR: HTTP Response string: ");
        myLogLn(resp.c_str());        
        ret = RET_ERR;
      } else {                                                // Only repoort errors.
        // myLogLn("HTTP OK");        
      }
    }
    client.stop();
    http.end();
  } else {
    myLog("HTTP ERROR: Wifi not connected when trying to POST data");
    ret = RET_ERR;
  }
  return ret;
}

/**
 *  ******************************************************
 * @brief   Maps logging to the desired streams.
 * @details This gives a bit more flexibility to logging.
 *          Currently, logging is provided to Serial and TelnetStream.
 *          This can definitively solved more elegant, but this way ist works...
 * @param   txt: The string to log
 * @return  none
 */
static void myLog(const char *txt) {
  Serial.print(txt);
  TelnetStream.print(txt);
}
static void myLogLn(const char *txt) {
  Serial.println(txt);
  TelnetStream.println(txt);
}
